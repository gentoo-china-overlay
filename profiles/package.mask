# Hong Hao <oahong@gmail.com> (28 Dec 2010)
# Use eapi 4, needs portage >=2.1.9.27
~app-i18n/fcitx-9999

# bones7456 <bones7456@gmail.com> (09 Feb 2011)
# Masked for removal in 30 days. 
# Portage app-portage/pfl already contain this program.
# please install app-portage/pfl instead.
app-portage/e-file
